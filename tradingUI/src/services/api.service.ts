import { Injectable } from '@angular/core';
import {Observable, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL:string = environment.rest_host + '/trade/';
  strategyURL:string = environment.rest_host + '/strategy';


  constructor(private http: HttpClient) {
  }


  getAllTrades() {
    return this.http.get(`${this.apiURL}`)
      .pipe(
        catchError(this.handleError<Object[]>('getParamData', []))
      );


  }

  getTradeWithID(id) {
    console.log(`${this.apiURL}/${id}`);

    return this.http.get(`${this.apiURL}/${id}`)
      .pipe(
        catchError(this.handleError<Object[]>('getParamDataWithId', []))
      );
  }

  addNewStrategy(data) {
    return this.http.post(`${this.strategyURL}`, data)
      .pipe(
        catchError(this.handleError('postNewStrategy', []))
      );
  }



  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return throwError('Something bad happened');
    };
  }
}
