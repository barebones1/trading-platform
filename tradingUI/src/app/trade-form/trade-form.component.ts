import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {FormsModule} from "@angular/forms";
import {MatSliderModule} from '@angular/material/slider';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {Router} from "@angular/router";

@Component({
  selector: 'app-trade-form',
  templateUrl: './trade-form.component.html',
  styleUrls: ['./trade-form.component.css']
})

export class TradeFormComponent implements OnInit {
  title = 'tradingUI';
  whichStock: string = 'stocks';
  stocks: Object[] = [{id: 1, ticker: 'AAPL'}, {id: 2, ticker: 'MSFT'}, {id: 3, ticker: 'GOOG'}, { id: 4 ,ticker: 'BRK-A'}, {id: 5  ,ticker: 'NSC'}]
  allTrades;
  stockSize
  exitStrategyValue

  selectedStrategy: string = 'Two Moving Averages';
  algo: Object[] = [
    {id: 1, name: "Two Moving Averages"},
    {id: 2, name: "Bollinger Bands"}
  ];

  sliderHider = true
  exitStrategy;

  constructor(private router: Router, private apiService: ApiService) {
  }

  ngOnInit() {
    this.getAllTrades()
  }

  getAllTrades() {
    this.apiService.getAllTrades().subscribe((result) => {
      this.allTrades = result
    })
  }

  postTrade(stockId,stockSize,exitSize) {
    let data = [stockId,this.stocks[stockId-1]["ticker"], stockSize ,exitSize]
    console.log(data)
    this.apiService.addNewStrategy(data).subscribe(next=>{
      this.router.navigate(['/dashboard']);
    })
  }



  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }

    if (value >= 1000) {
      return Math.round(value / 1000) + '%';
    }

    return value;
  }



}


