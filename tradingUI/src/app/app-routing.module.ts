import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TradeFormComponent} from "./trade-form/trade-form.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {EodTradeComponent} from "./eod-trade/eod-trade.component";


const routes: Routes = [
  {path:'dashboard',component:DashboardComponent},
  {path:'book_trade',component:TradeFormComponent},
  {path:'eod_trade',component:EodTradeComponent},
  {path:'**',redirectTo:"/dashboard", pathMatch:'full'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
