import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from "@angular/material";
import {FormsModule} from "@angular/forms";
import {MatSliderModule} from '@angular/material/slider';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  displayedColumns: string[] = ['tradeID', 'ticker', 'tradePrice', 'tradeSize', 'tradeType', 'state', 'LastStateChange', 'action'];
  allTrades;
  singleTrade;
  dataSource;
  id;
  showEditTrade=true;
  whichStock:string =""

  stocks: Object[] = [{id: 1, ticker: 'AAPL'}, {id: 2, ticker: 'MSFT'}, {id: 3, ticker: 'GOOG'}, { id: 4 ,ticker: 'BRK-A'}, {id: 5  ,ticker: 'NSC'}]

  exitStrategy=2
  selectedStrategy
  sliderHider = true
  exitStrategyValue

  elements: any = [];


  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.getAllTrades()
    this.dataSource = new MatTableDataSource(this.allTrades);

  }

  getAllTrades() {
    this.apiService.getAllTrades().subscribe((result) => {
      this.allTrades = result;
      this.refreshData()
    })
  }

  refreshData(){
      setInterval(() => {
        this.getAllTrades();
        //Passing the false flag would prevent page reset to 1 and hinder user interaction
      }, 10000);
  }

  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }

    if (value >= 1000) {
      return Math.round(value / 1000) + '%';
    }

    return value;
  }

  showTrades(){
    this.showEditTrade=true

  }

  editTrade(id){
    this.apiService.getTradeWithID(id).subscribe((results)=>{
      this.singleTrade=results
      console.log(this.singleTrade)
    })
    this.showEditTrade=false
    this.exitStrategyValue=this.singleTrade.strategy.exitProfitLoss
  }
}
