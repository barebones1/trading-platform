import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-eod-trade',
  templateUrl: './eod-trade.component.html',
  styleUrls: ['./eod-trade.component.css']
})
export class EodTradeComponent implements OnInit {
  displayedColumns: string[] = ['tradeID', 'ticker', 'tradePrice', 'tradeSize', 'tradeType', 'state', 'LastStateChange'];
  allTrades;

  constructor(private apiService:ApiService) {
  }

  ngOnInit() {
    this.getAllTrades()
  }



  getAllTrades() {
    this.apiService.getAllTrades().subscribe((result) => {
      this.allTrades = result
    })
  }
}
