import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EodTradeComponent } from './eod-trade.component';

describe('EodTradeComponent', () => {
  let component: EodTradeComponent;
  let fixture: ComponentFixture<EodTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EodTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EodTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
