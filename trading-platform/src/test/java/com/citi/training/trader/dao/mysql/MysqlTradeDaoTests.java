package com.citi.training.trader.dao.mysql;

import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

/**
 * JUnit Test for MysqlTradeDao. Testing the save, find all, find by latest
 * strategy id and delete by id methods. Also tested the else conditions.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {

	@Autowired
	MysqlSimpleStrategyDao mysqlSimpleStrategyDao;

	@Autowired
	MysqlStockDao mysqlStockDao;

	@Autowired
	MysqlTradeDao mysqlTradeDao;

	@XmlTransient
	private LocalDateTime lastStateChange = LocalDateTime.now();

	private Stock testStock;
	private Trade testTrade;
	private SimpleStrategy testSimpleStrategy;

	@Before
	public void setup() throws Exception {
		testStock = new Stock(-1, "AMAZ");
	}

	@Test
	@Transactional
	public void test_saveAndFindall() {

		int id = mysqlStockDao.create(testStock);
		testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(id), 100);
		testTrade = new Trade(10.0, 100, TradeType.BUY, testSimpleStrategy);
		mysqlSimpleStrategyDao.save(testSimpleStrategy);
		mysqlTradeDao.save(testTrade);

		List<Trade> trades = mysqlTradeDao.findAll();
		assertTrue(trades.size() >= 1);

	}

	@Test
	@Transactional
	public void test_findLatestByStrategyId() {

		int id = mysqlStockDao.create(testStock);
		testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(id), 100);
		int strategyId = mysqlSimpleStrategyDao.save(testSimpleStrategy);

		testTrade = new Trade(10.0, 100, TradeType.BUY, mysqlSimpleStrategyDao.findById(strategyId));
		mysqlSimpleStrategyDao.save(testSimpleStrategy);
		mysqlTradeDao.save(testTrade);

		Trade returnedTrade = mysqlTradeDao.findLatestByStrategyId(strategyId);
		assert (returnedTrade.getStrategy().getId() == strategyId);

	}

	@Test
	@Transactional
	public void test_findAllByState() {

		int id = mysqlStockDao.create(testStock);
		testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(id), 100);
		testTrade = new Trade(10.0, 100, TradeType.BUY, testSimpleStrategy);
		mysqlSimpleStrategyDao.save(testSimpleStrategy);
		mysqlTradeDao.save(testTrade);

		Trade.TradeState state = TradeState.INIT;

		List<Trade> trades = new ArrayList<Trade>();
		trades = mysqlTradeDao.findAllByState(state);

		assert (trades.get(0).getState() == state);
	}

	@Test
	@Transactional
	public void test_DeleteById() {

		int id = mysqlStockDao.create(testStock);
		testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(id), 100);
		testTrade = new Trade(10.0, 100, TradeType.BUY, testSimpleStrategy);
		mysqlSimpleStrategyDao.save(testSimpleStrategy);
		mysqlTradeDao.save(testTrade);

		mysqlTradeDao.deleteById(testTrade.getId());

	}
	
	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void test_DeleteByIdUnsuccessful() {

		mysqlTradeDao.deleteById(213321123);

	}
	
	@Test
	@Transactional
	public void test_saveUpdate() {
		int id = mysqlStockDao.create(testStock);
		testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(id), 100);
		testTrade = new Trade(10.0, 100, TradeType.BUY, testSimpleStrategy);
		mysqlSimpleStrategyDao.save(testSimpleStrategy);
		mysqlTradeDao.save(testTrade);
		mysqlTradeDao.save(testTrade);
		
		List<Trade> trades = mysqlTradeDao.findAll();
		assertTrue(trades.size() >= 1);
		
	}

}
