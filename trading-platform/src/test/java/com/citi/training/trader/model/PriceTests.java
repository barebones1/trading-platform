package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import org.junit.Test;

/**
 * JUnit Test for Price model class. Testing the constructor and to string
 * methods.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

public class PriceTests {

	private int testId = 5;
	private Stock testStock = new Stock(6, "fb");
	private double testPrice = 10.0;
	private Date testRecordedAt = new Date();

	@Test
	public void test_Price_constructor() {
		Price testNewPrice = new Price(testId, testStock, testPrice, testRecordedAt);

		assertEquals(testId, testNewPrice.getId());
		assertEquals(testStock, testNewPrice.getStock());
		assertEquals(testPrice, testNewPrice.getPrice(), 0.2);
		assertEquals(testRecordedAt, testNewPrice.getRecordedAt());
	}

	@Test
	public void test_ToString() {
		String testNewPrice = new Price(testId, testStock, testPrice, testRecordedAt).toString();

		assertTrue(testNewPrice.contains(new Integer(testId).toString()));
		assertTrue(testNewPrice.contains((new Stock(testStock.getId(), testStock.getTicker()).toString())));
		assertTrue(testNewPrice.contains(new Double(testPrice).toString()));
		assertTrue(testNewPrice.contains(new Date().toString()));

	}

}
