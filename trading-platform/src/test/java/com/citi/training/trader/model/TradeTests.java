package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;

import org.junit.Test;

import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

/**
 * JUnit Test for Trade model class. Testing the constructor and to string
 * methods. Problem when trying to check strategy in to string mehtod. Will be
 * fixed in version 1.1.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

public class TradeTests {

	LocalDateTime rightNow = LocalDateTime.now();
	private Stock testStock = new Stock(6, "fb");
	private int testId = 4;
	private double testPrice = 5.4;
	private int testSize = 20;
	private TradeType testTradeType = TradeType.BUY;
	private TradeState testTradeState = TradeState.INIT;
	private LocalDateTime testLastStateChange = rightNow;
	private boolean testAccountedFor = true;
	private SimpleStrategy testStrategy = new SimpleStrategy(testStock, 12);

	@Test
	public void test_Trade_constructor() {
		Trade testNewTrade = new Trade(testId, testPrice, testSize, testTradeType.toString(), testTradeState.toString(),
		        testLastStateChange, testAccountedFor, testStrategy);

		assertEquals(testId, testNewTrade.getId(), 0.7); // query tomorrow
		assertEquals(testPrice, testNewTrade.getPrice(), 0.2);
		assertEquals(testSize, testNewTrade.getSize());
		assertEquals(testTradeType, testNewTrade.getTradeType());
		assertEquals(testTradeState, testNewTrade.getState());
		assertEquals(testLastStateChange, testNewTrade.getLastStateChange());
		assertEquals(testAccountedFor, testNewTrade.getAccountedFor());
		assertEquals(testStrategy, testNewTrade.getStrategy());
	}

	@Test
	public void test_ToString() {
		String testNewTrade = new Trade(testId, testPrice, testSize, testTradeType.toString(),
		        testTradeState.toString(), testLastStateChange, testAccountedFor, testStrategy).toString();

		assertTrue(testNewTrade.contains(new Integer(testId).toString()));
		assertTrue(testNewTrade.contains(testTradeType.toString()));
		assertTrue(testNewTrade.contains(new Double(testPrice).toString()));
		assertTrue(testNewTrade.contains(new Integer(testSize).toString()));
		assertTrue(testNewTrade.contains(testTradeState.toString()));
		assertTrue(testNewTrade.contains(testLastStateChange.toString()));
		assertTrue(testNewTrade.contains(new Boolean(testAccountedFor).toString()));
		// assertTrue(testNewTrade.contains((new
		// SimpleStrategy(testStrategy.getStock(),testStrategy.getSize()).toString())));
	}
}
