package com.citi.training.trader.dao.mysql;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.StockNotFoundException;
import com.citi.training.trader.model.Stock;

/**
 * JUnit Test for MysqlStockDao. Testing the create, find all, find by id, find
 * by ticker and delete by id methods.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlStockDaoTests {

	@Autowired
	MysqlStockDao mysqlStockDao;

	private Stock testStock;

	@Before
	public void setup() throws Exception {
		testStock = new Stock(1, "AMAZ");

	}

	@Test
	@Transactional
	public void test_createAndFindall() {

		mysqlStockDao.create(testStock);
		List<Stock> stocks = mysqlStockDao.findAll();
		assertTrue(stocks.size() >= 1);
	}

	@Test
	@Transactional
	public void test_findById() {

		int stockId = mysqlStockDao.create(testStock);

		Stock stocks = mysqlStockDao.findById(stockId);

		assert (stocks.getId() == stockId);

	}

	@Test
	@Transactional
	public void test_findByTicker() {

		mysqlStockDao.create(testStock);

		String ticker = testStock.getTicker();

		List<Stock> stocks = new ArrayList<Stock>();
		stocks = mysqlStockDao.findAll();
		assert (stocks.get(0).getTicker() == ticker);
	}

	@Test
	@Transactional
	public void test_deleteById() {

		int id = mysqlStockDao.create(testStock);

		mysqlStockDao.deleteById(id);
	}
	
	@Test (expected = StockNotFoundException.class)
	@Transactional
	public void test_deleteByIdUnsuccessful() {

		mysqlStockDao.deleteById(333434342);
	}
	
}
