package com.citi.training.trader.service;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.mysql.MysqlStockDao;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;

/**
 * JUnit Test for Simple Strategy Service class. Testing the find all and save
 * method.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class SimpleStrategyServiceTests {

	@Autowired
	SimpleStrategyService simpleStrategyService;

	@Autowired
	MysqlStockDao mysqlStockDao;

	private SimpleStrategy simpleStrategy;
	private Stock testStock;

	@Before
	public void setUp() throws Exception {
		testStock = new Stock(1, "AMAZ");
	}

	@Test
	@Transactional
	public void testFindAllAndSave() {
		int stockId = mysqlStockDao.create(testStock);
		simpleStrategy = new SimpleStrategy(mysqlStockDao.findById(stockId), 100);
		simpleStrategyService.save(simpleStrategy);

		List<SimpleStrategy> strategies = simpleStrategyService.findAll();
		assert (strategies.size() > 0);

	}
}
