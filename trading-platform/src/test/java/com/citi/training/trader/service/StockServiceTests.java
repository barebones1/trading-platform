package com.citi.training.trader.service;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.StockNotFoundException;
import com.citi.training.trader.model.Stock;

/**
 * JUnit Test for Stock Service class. Testing the test create, find all, find
 * by id, find by ticker and delete by id method.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class StockServiceTests {

	@Autowired
	StockService stockService;

	private Stock testStock;

	@Before
	public void setUp() throws Exception {
		testStock = new Stock(1, "AMAZ");
	}

	@Test
	@Transactional
	public void testCreateAndFindAll() {
		stockService.create(testStock);
		List<Stock> stocks = stockService.findAll();
		assert (stocks.size() > 0);
	}

	@Test
	@Transactional
	public void testFindById() {
		int stockId = stockService.create(testStock);
		Stock stocks = stockService.findById(stockId);
		assert (stocks.getId() == stockId);
		assert (stocks.getTicker() == "AMAZ");
	}

	@Test
	@Transactional
	public void testFindByTicker() {
		int stockId = stockService.create(testStock);
		Stock stocks = stockService.findByTicker(testStock.getTicker());
		assert (stocks.getId() == stockId);
		assert (stocks.getTicker() == "AMAZ");
	}

	@Test(expected = StockNotFoundException.class)
	@Transactional
	public void testDeleteById() {
		int stockId = stockService.create(testStock);
		stockService.deleteById(stockId);
		stockService.findById(stockId);
	}

}
