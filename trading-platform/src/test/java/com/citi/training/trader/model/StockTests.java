package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * JUnit Test for Stock model class. Testing the constructor and to string
 * methods.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

public class StockTests {

	private int testId = 6;
	private String testTicker = "AMAZ";

	@Test
	public void test_Stock_Controller() {
		Stock newStock = new Stock(testId, testTicker);

		assertEquals(testId, newStock.getId());
		assertEquals(testTicker, newStock.getTicker());
	}

	@Test
	public void test_ToString() {
		String newStock = new Stock(testId, testTicker).toString();

		assertTrue(newStock.contains(new Integer(testId).toString()));
		assertTrue(newStock.contains(testTicker));

	}
}
