package com.citi.training.trader.service;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

/**
 * JUnit Test for Price Reader Service class. Testing the test create, find all
 * and delete older than method.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class PriceServiceTests {

	@Autowired
	private PriceService priceService;

	@Autowired
	private StockService stockService;

	private Stock testStock;
	private Price testPrice;

	@Before
	public void setUp() throws Exception {
		testStock = new Stock(1, "AMAZ");

	}

	@Test
	@Transactional
	public void testCreate() {
		int stockId = stockService.create(testStock);
		testPrice = new Price(stockService.findById(stockId), 10);

		int id = priceService.create(testPrice);
		List<Price> prices = priceService.findLatest(testStock, 1);
		assert (prices.get(0).getId() == id);

	}

	@Test
	@Transactional
	public void testFindAll() {
		int stockId = stockService.create(testStock);
		testPrice = new Price(stockService.findById(stockId), 10);

		priceService.create(testPrice);
		List<Price> prices = priceService.findAll(stockService.findById(stockId));
		assertTrue(prices.size() > 0);
	}

//	@Test
//	@Transactional
//	public void testDeleteOlderThan() {
//
//		int stockId = stockService.create(testStock);
//		testPrice = new Price(stockService.findById(stockId), 10);
//		priceService.create(testPrice);
//		priceService.deleteOlderThan(new Date());
//
//		try {
//			Thread.sleep(7000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		List<Price> prices = priceService.findAll(stockService.findById(stockId));
//		assertTrue(prices.size() == 0);
//
//	}

}
