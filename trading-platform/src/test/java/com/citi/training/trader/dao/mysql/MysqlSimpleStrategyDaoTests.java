package com.citi.training.trader.dao.mysql;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;

/**
 * JUnit Test for MysqlSimpleStrategyDao. Testing the save, find all, find by id
 * methods.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlSimpleStrategyDaoTests {

	@Autowired
	MysqlSimpleStrategyDao mysqlSimpleStrategyDao;

	@Autowired
	MysqlStockDao mysqlStockDao;

	private Stock testStock;
	private SimpleStrategy testSimpleStrategy;
	// private Date date;

	@Before
	public void setup() throws Exception {
		testStock = new Stock(-1, "AMAZ");
		// testSimpleStrategy = new SimpleStrategy(1, testStock, 100, 10.0, 1, 25.0,
		// 250.0, date);

	}

	@Test
	@Transactional
	public void test_saveAndFindAll() {

		int id = mysqlStockDao.create(testStock);
		testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(id), 100);
		mysqlSimpleStrategyDao.save(testSimpleStrategy);

		List<SimpleStrategy> simpleStrategies = mysqlSimpleStrategyDao.findAll();
		assertTrue(simpleStrategies.size() >= 1);

	}

	@Test
	@Transactional
	public void test_findById() {

		int id = mysqlStockDao.create(testStock);
		testSimpleStrategy = new SimpleStrategy(mysqlStockDao.findById(id), 100);
		mysqlSimpleStrategyDao.save(testSimpleStrategy);

		int stratid = testSimpleStrategy.getId();

		List<SimpleStrategy> strategies = new ArrayList<SimpleStrategy>();
		strategies = mysqlSimpleStrategyDao.findAll();
		assert (strategies.get(0).getId() == stratid);
	}
}
