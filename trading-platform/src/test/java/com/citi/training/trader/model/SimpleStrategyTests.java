package com.citi.training.trader.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.Date;
import org.junit.Test;

/**
 * JUnit Test for Simple Strategy model class. Testing the constructor and to
 * string methods.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

public class SimpleStrategyTests {

	private int testId = 6;
	private Stock testStock = new Stock(6, "fb");
	private int testSize = 100;
	private double testExitProfitLoss = 5.5;
	private int testCurrentPosition = 1;
	private double testLastTradePrice = 45.4;
	private double testProfit = 32.2;
	private Date testStopped = new Date();

	@Test
	public void test_SimpleStrategy_Constructor() {
		SimpleStrategy testSimpleStrategy = new SimpleStrategy(testId, testStock, testSize, testExitProfitLoss,
		        testCurrentPosition, testLastTradePrice, testProfit, testStopped);

		assertEquals(testId, testSimpleStrategy.getId());
		assertEquals(testStock, testSimpleStrategy.getStock());
		assertEquals(testSize, testSimpleStrategy.getSize());
		assertEquals(testExitProfitLoss, testSimpleStrategy.getExitProfitLoss(), 0.2);
		assertEquals(testCurrentPosition, testSimpleStrategy.getCurrentPosition());
		assertEquals(testLastTradePrice, testSimpleStrategy.getLastTradePrice(), 0.2);
		assertEquals(testProfit, testSimpleStrategy.getProfit(), 0.2);
		assertEquals(testStopped, testSimpleStrategy.getStopped());

	}

	@Test
	public void test_ToString() {
		String testSimpleStrategy = new SimpleStrategy(testId, testStock, testSize, testExitProfitLoss,
		        testCurrentPosition, testLastTradePrice, testProfit, testStopped).toString();

		assertTrue(testSimpleStrategy.contains(new Integer(testId).toString()));
		assertTrue(testSimpleStrategy.contains(new Stock(testStock.getId(), testStock.getTicker()).toString()));
		assertTrue(testSimpleStrategy.contains(new Integer(testSize).toString()));
		assertTrue(testSimpleStrategy.contains(new Double(testExitProfitLoss).toString()));
		assertTrue(testSimpleStrategy.contains(new Integer(testCurrentPosition).toString()));
		assertTrue(testSimpleStrategy.contains(new Date().toString()));
	}

}