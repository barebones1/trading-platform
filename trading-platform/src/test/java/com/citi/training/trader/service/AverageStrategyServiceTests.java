package com.citi.training.trader.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.dao.mysql.MysqlPriceDao;
import com.citi.training.trader.dao.mysql.MysqlStockDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

/**
 * JUnit Test for Average Strategy Service class. Testing the get average price
 * method.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class AverageStrategyServiceTests {

	@Autowired
	AverageStrategyService averageStrategyService;

	@Autowired
	MysqlPriceDao mysqlPriceDao;

	@Autowired
	MysqlStockDao mysqlStockDao;

	private Stock testStock;
	private Price testPrice;
	private Price testPrice1;

	@Before
	public void setUp() throws Exception {

		testStock = new Stock(1, "AMAZ");
	}

	@Test
	@Transactional
	public void testGetPriceAverage() {

		int id = mysqlStockDao.create(testStock);
		testPrice = new Price(mysqlStockDao.findById(id), 10.0);
		testPrice1 = new Price(mysqlStockDao.findById(id), 12.0);
		mysqlPriceDao.create(testPrice);
		mysqlPriceDao.create(testPrice1);

		List<Price> prices = mysqlPriceDao.findLatest(testStock, 10);
		double average = averageStrategyService.getPriceAverage(prices);

		assertTrue(averageStrategyService.getPriceAverage(prices) == average);

	}

}
