package com.citi.training.trader.service;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import com.citi.training.trader.dao.mysql.MysqlSimpleStrategyDao;
import com.citi.training.trader.dao.mysql.MysqlStockDao;
import com.citi.training.trader.dao.mysql.MysqlTradeDao;
import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

/**
 * JUnit Test for Trade Service class. Testing the test save, find all, find by
 * id, find all by state, find latest by strategy id and delete id method.
 * 
 * @author Conor Burns
 * @author BareBones
 * @version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class TradeServiceTests {

	@Autowired
	private TradeService tradeService;

	@Autowired
	MysqlStockDao mysqlStockDao;

	@Autowired
	MysqlTradeDao mysqlTradeDao;

	@Autowired
	MysqlSimpleStrategyDao mysqlSimpleStrategyDao;

	private Trade testTrade;
	private Stock testStock;
	private SimpleStrategy simpleStrategy;

	private double price = 10.0;
	private int size = 100;
	private TradeType tradeType = TradeType.BUY;

	@Before
	public void setUp() throws Exception {
		testStock = new Stock(1, "AMAZ");
	}

	@Test
	@Transactional
	public void testSaveAndFindAll() {
		int stockId = mysqlStockDao.create(testStock);
		simpleStrategy = new SimpleStrategy(mysqlStockDao.findById(stockId), size);
		int strategyId = mysqlSimpleStrategyDao.save(simpleStrategy);
		testTrade = new Trade(price, stockId, tradeType, mysqlSimpleStrategyDao.findById(strategyId));
		tradeService.save(testTrade);

		List<Trade> trades = tradeService.findAll();
		assert (trades.size() >= 1);
	}

	@Test
	@Transactional
	public void testFindById() {
		int stockId = mysqlStockDao.create(testStock);
		simpleStrategy = new SimpleStrategy(mysqlStockDao.findById(stockId), size);
		int strategyId = mysqlSimpleStrategyDao.save(simpleStrategy);
		testTrade = new Trade(price, stockId, tradeType, mysqlSimpleStrategyDao.findById(strategyId));
		int tradeId = tradeService.save(testTrade);

		Trade returnedTrade = tradeService.findById(tradeId);
		assert (returnedTrade.getId() == tradeId);
	}

	@Test
	@Transactional
	public void testFindAllByState() {
		int stockId = mysqlStockDao.create(testStock);
		simpleStrategy = new SimpleStrategy(mysqlStockDao.findById(stockId), size);
		int strategyId = mysqlSimpleStrategyDao.save(simpleStrategy);
		testTrade = new Trade(price, stockId, tradeType, mysqlSimpleStrategyDao.findById(strategyId));
		tradeService.save(testTrade);

		List<Trade> trades = tradeService.findAllByState(TradeState.INIT);
		assert (trades.size() > 0);
	}

	@Test
	@Transactional
	public void testFindLatestByStrategyId() {
		int stockId = mysqlStockDao.create(testStock);
		simpleStrategy = new SimpleStrategy(mysqlStockDao.findById(stockId), size);
		int strategyId = mysqlSimpleStrategyDao.save(simpleStrategy);
		testTrade = new Trade(price, stockId, tradeType, mysqlSimpleStrategyDao.findById(strategyId));
		tradeService.save(testTrade);

		Trade returnedTrade = tradeService.findLatestByStrategyId(strategyId);
		assert (returnedTrade.getStrategy().getId() == strategyId);
	}

	@Test(expected = TradeNotFoundException.class)
	@Transactional
	public void testDeleteById() {
		int stockId = mysqlStockDao.create(testStock);
		simpleStrategy = new SimpleStrategy(mysqlStockDao.findById(stockId), size);
		int strategyId = mysqlSimpleStrategyDao.save(simpleStrategy);
		testTrade = new Trade(price, stockId, tradeType, mysqlSimpleStrategyDao.findById(strategyId));
		int tradeId = tradeService.save(testTrade);

		tradeService.deleteById(tradeId);
		tradeService.findById(tradeId);
	}

}
