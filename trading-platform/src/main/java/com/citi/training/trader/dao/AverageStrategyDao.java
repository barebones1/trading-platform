package com.citi.training.trader.dao;

import com.citi.training.trader.model.SimpleStrategy;

import java.util.List;

public interface AverageStrategyDao {

    List<SimpleStrategy> findAll();
}
