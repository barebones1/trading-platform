package com.citi.training.trader.dao.mysql;

import com.citi.training.trader.dao.AverageStrategyDao;
import com.citi.training.trader.model.SimpleStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MysqlAverageStrategyDao implements AverageStrategyDao {

    private static final Logger logger =
            LoggerFactory.getLogger(MysqlSimpleStrategyDao.class);

    private static String FIND_ALL_SQL = "select simple_strategy.id as strategy_id, stock.id as stock_id, stock.ticker, " +
            "size, exit_profit_loss, current_position, last_trade_price, profit, stopped " +
            "from simple_strategy left join stock on stock.id = simple_strategy.stock_id";

    private static String INSERT_SQL = "INSERT INTO simple_strategy (stock_id, size, exit_profit_loss, current_position, " +
            "last_trade_price, profit, stopped) " +
            "values (:stock_id, :size, :exit_profit_loss, :current_position, "+
            ":last_trade_price, :profit, :stopped)";

    private static String UPDATE_SQL = "UPDATE simple_strategy set stock_id=:stock_id, size=:size, " +
            "exit_profit_loss=:exit_profit_loss, current_position=:current_position, " +
            "last_trade_price=:last_trade_price, profit=:profit, stopped=:stopped where id=:id";

    @Autowired
    private JdbcTemplate tpl;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public List<SimpleStrategy> findAll() {
        return null;
    }
}
