package com.citi.training.trader.rest;

import com.citi.training.trader.model.SimpleStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.service.SimpleStrategyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host:http://localhost:4200}")
@RestController
@RequestMapping("${com.citi.training.trader.rest.stock-base-path:/api/strategy}")
public class StrategyController {

    private static final Logger logger =
            LoggerFactory.getLogger(StockController.class);

    @Autowired
    private SimpleStrategyService simpleStrategy;
    SimpleStrategy strategy;

    @RequestMapping(method= RequestMethod.POST,
            consumes= MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<SimpleStrategy> create(@RequestBody Object[] data) {
        logger.debug("create(" + data + ")");

        Stock stock = new Stock((int)data[0],(String)data[1]);
        strategy=new SimpleStrategy(-1,stock,(int)data[2], (double) (int) data[3],0,0,0,null);
        strategy.setId(simpleStrategy.save(strategy));
        logger.debug("created stock: " + strategy);

        return new ResponseEntity<SimpleStrategy>(strategy, HttpStatus.CREATED);
    }
}
